package com.sp.osf.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sp.osf.service.OsfMemberService;
import com.sp.osf.vo.OsfMemberVO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
public class OsfMemberController {

	@Resource
	private OsfMemberService oms;
	
	@GetMapping("/members")
	public List<OsfMemberVO> findMemberList(OsfMemberVO oMember){
		log.info("oMember=>{}",oMember);
		return oms.selectOsfMemberList(oMember);
	}
	
	@GetMapping("/member/{omId}")
	public OsfMemberVO findMemberById(@PathVariable("omId") String omId) {
		return oms.selectOsfMemberById(omId);
	}
	
	@PostMapping("/member")
	public int addMember(OsfMemberVO oMember){
		log.info("oMember=>{}",oMember);
		String path = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/upload/")
				.toUriString();
		log.info("path=>{}",path);
		return oms.insertOsfMember(oMember);
	}
	@PostMapping("/member/{omId}")
	public int modifyMember(OsfMemberVO oMember){
		log.info("oMember=>{}",oMember);
		return oms.insertOsfMember(oMember);
	}

	@PostMapping("/login")
	public OsfMemberVO selectMemberByIdAndPwd(@RequestBody OsfMemberVO member) {
		log.debug("login => {}",member);
		return oms.selectOsfMemberByIdAndPwd(member);
	}
	@DeleteMapping("/member/{omNo}")
	public Integer deleteMember(@PathVariable("omNo") Integer omNo) {
		log.debug("omNo => {}",omNo);
		return oms.deleteMemberByNo(omNo);
	}
	@PostMapping("/member/modi")
	public Integer modifyOsfMember(OsfMemberVO member) {
		log.debug("update => {}",member);
		return oms.updateMember(member);
	}
}
