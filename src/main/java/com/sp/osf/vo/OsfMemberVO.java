package com.sp.osf.vo;

import org.apache.ibatis.type.Alias;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Alias("osfmem")
@Data
public class OsfMemberVO {

	private int omNo;
	private String omDevice;
	private String omId;
	private String omNick;
	private String omPass;
	private String omMail;
	private String omProfile;
	
	private MultipartFile omProfileFile;
	
	private String omPhone;
	private String omBirth;
	private String omTrans;
	private String omToken;
	private String omZipcode;
	private String omAddr1;
	private String omAddr2;
	private String omInfo;

}
