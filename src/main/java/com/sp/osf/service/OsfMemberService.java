package com.sp.osf.service;

import java.util.List;

import com.sp.osf.vo.OsfMemberVO;

public interface OsfMemberService {
	public List<OsfMemberVO> selectOsfMemberList(OsfMemberVO oMember);
	public OsfMemberVO selectOsfMember(int omNo);
	public OsfMemberVO selectOsfMemberById(String omId);
	public Integer insertOsfMember(OsfMemberVO oMember);
	public Integer updateOsfMember(OsfMemberVO oMember);
	public Integer deleteOsfMember(OsfMemberVO oMember);
	public OsfMemberVO selectOsfMemberByIdAndPwd(OsfMemberVO oMember);
	public Integer deleteMemberByNo(Integer omNo);
	public Integer updateMember(OsfMemberVO member);
	public Integer updateMember2(OsfMemberVO member);
}
