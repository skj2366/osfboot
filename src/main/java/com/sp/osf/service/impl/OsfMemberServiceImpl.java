package com.sp.osf.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sp.osf.mapper.OsfMemberMapper;
import com.sp.osf.service.OsfMemberService;
import com.sp.osf.vo.OsfMemberVO;

@Service
public class OsfMemberServiceImpl implements OsfMemberService {

	@Resource
	private OsfMemberMapper omm;

	private final String BASE = "C:\\Users\\Administrator\\git\\osfboot\\src\\main\\webapp\\resources\\upload\\";

	@Override
	public List<OsfMemberVO> selectOsfMemberList(OsfMemberVO oMember) {
		return omm.selectOsfMemberList(oMember);
	}

	@Override
	public OsfMemberVO selectOsfMember(int omNo) {
		return null;
	}

	@Override
	public Integer insertOsfMember(OsfMemberVO oMember) {
		MultipartFile omProfileFile = oMember.getOmProfileFile();

		String fileName = omProfileFile.getOriginalFilename();
		String extName = FilenameUtils.getExtension(fileName);
		String reName = Long.toString(System.nanoTime());
		reName += "." + extName;

		File targetFile = new File(BASE + reName);

		try {
			Files.copy(omProfileFile.getInputStream(), targetFile.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}

		oMember.setOmProfile(reName);
		return omm.insertMember(oMember);
	}

	@Override
	public Integer updateOsfMember(OsfMemberVO oMember) {
		return null;
	}

	@Override
	public Integer deleteOsfMember(OsfMemberVO oMember) {
		return null;
	}

	@Override
	public OsfMemberVO selectOsfMemberById(String omId) {
		return omm.selectOsfMemberById(omId);
	}

	@Override
	public OsfMemberVO selectOsfMemberByIdAndPwd(OsfMemberVO oMember) {
		return omm.selectOsfMemberByIdAndPwd(oMember);
	}

	@Override
	public Integer deleteMemberByNo(Integer omNo) {
		OsfMemberVO orgMember = omm.selectOsfMember(omNo);
		if (omm.deleteMemberByNo(omNo) == 1) {
			if (orgMember.getOmProfile() != null) {
				File orgFile = new File(BASE + orgMember.getOmProfile());
				if (orgFile.exists()) {
					orgFile.delete();
				}
			}
			return 1;
		}
		return null;
//		return omm.deleteMemberByNo(omNo);

	}

	@Override
	public Integer updateMember(OsfMemberVO member) {

		MultipartFile omProfileFile = member.getOmProfileFile();
		String fileName = omProfileFile.getOriginalFilename();
		String extName = FilenameUtils.getExtension(fileName);
		String reName = Long.toString(System.nanoTime());
		reName += "." + extName;

		File targetFile = new File(BASE + reName);

		try {
			Files.copy(omProfileFile.getInputStream(), targetFile.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}

		member.setOmProfile(reName);
		OsfMemberVO orgMember = omm.selectOsfMemberById(member.getOmId());// 전에있는 데이터 우선 조회
		System.out.println(member.getOmProfileFile().isEmpty());
		if (omm.updateOsfMember(member) == 1) { // 새로운걸 업데이트 하고 1이되었을경우 ( 업데이트가 잘 됐을 경우)
			if (orgMember.getOmProfile() != null) {
				File orgFile = new File(BASE + orgMember.getOmProfile());// 방금전에 조회했던것에서 proFile을 삭제
				if (orgFile.exists()) {
					orgFile.delete();
				} // 만약에 이 로직을 넣지 않으면 이미지가 계~속 늘어난다.
			}
			return 1;

		} else {// 정상적이로 update가 안되었을때는 타겟파일을 삭제하는 로직 .
			targetFile.delete();
		}
		// 완벽한 로직은 아니다. 제대로 하려면 전부다 try catch를 해서 상황에 대한 처리를 해주어야한다. 이 방식은 그나마 문제를 줄인거고
		// 사용하다 보면 100퍼센트 문제가 생긴다고 함 .

		return null;
	}

	@Override
	public Integer updateMember2(OsfMemberVO member) {
		return null;
	}

}
