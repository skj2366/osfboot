package com.sp.osf.mapper;

import java.util.List;

import com.sp.osf.vo.OsfMemberVO;

public interface OsfMemberMapper {
	public List<OsfMemberVO> selectOsfMemberList(OsfMemberVO oMember);
	public OsfMemberVO selectOsfMember(int omNo);
	public OsfMemberVO selectOsfMemberById(String omId);
	public Integer insertMember(OsfMemberVO oMember);
	public Integer updateOsfMember(OsfMemberVO oMember);
	public Integer deleteOsfMember(OsfMemberVO oMember);
	public OsfMemberVO selectOsfMemberByIdAndPwd(OsfMemberVO oMember);
	public Integer deleteMemberByNo(Integer omNo);
	public Integer updateMember(OsfMemberVO member);
}
