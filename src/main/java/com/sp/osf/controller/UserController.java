package com.sp.osf.controller;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
public class UserController {
	
	@GetMapping("/users")
	public List<Map<String,String>> getUserList(){
		
		return null;
	}
	
	@GetMapping("/user/1")
	public Map<String,String> getUser(@PathVariable int userNo){
		userNo = 1;
		return null;
	}
	
	@PostMapping("/user")
	public Integer insertUser(Map<String,String> map) {
		return 1;
	}
	
	@DeleteMapping("/user/1")
	public Integer deleteUser(@PathVariable int userNo) {
		return 1;
	}

}
